[core]
	pager = LESSCHARSET=utf-8 diff-so-fancy | less --tabs=4 -RFX
	editor = nvim
[color]
	ui = true
[color "diff-highlight"]
	oldNormal = red bold
	oldHighlight = red bold 52
	newNormal = green bold
	newHighlight = green bold 22
[color "diff"]
	meta = 11
	frag = magenta bold
	commit = yellow bold
	old = red bold
	new = green bold
	whitespace = red reverse
	func = 146 bold
[alias]
	co = checkout --recurse-submodules
	ds = diff --staged
	st = status --short
	ci = commit
	df = diff
	glog = log --graph --pretty=format:'%Cred%h%Creset - %s %Cgreen(%cr) %C(cyan)<%an>%Creset%C(yellow)%d%Creset' --abbrev-commit --date=relative
	crnt = rev-parse --abbrev-ref HEAD
	in = "!git fetch origin && git log $(git crnt)..origin/$(git crnt)"
	out = "!git glog --branches --not --remotes=origin"
	fresh = clean -f -d -X
	fixup = commit --amend --no-edit
	count = "!git log --oneline --branches --tags | wc -l"
[merge]
	tool = diffconflicts
	conflictstyle = diff3
[mergetool]
	prompt = false
	keepBackup = false
[push]
	followTags = true
	default = current
	autoSetupRemote = true
[submodule]
	recurse = true
[diff]
	ignoreSubmodules = dirty
[pull]
	rebase = false
[mergetool "diffconflicts"]
	cmd = nvim -c DiffConflicts \"$MERGED\"
	trustExitCode = true
[init]
	defaultBranch = master
[rerere]
	enabled = true
